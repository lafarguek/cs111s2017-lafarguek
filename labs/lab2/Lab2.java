
//*******************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kadeem LaFargue
//CMPSC 111 Spring 2017
//Lab #2
//Date: 01 26 2017
//
//Purpose: To calculate the distance and ho
//*******************************
import java.util.Date; // needed for printing today's date

public class Lab2
{
	//-------------------------
	//main method: program execution begins here
	//-------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kadeem LaFargue\nLab 2\n" + new Date() + "\n");

		// Variable dictionary:
		int ydsToWonderland = 864585;	// distance to Wonderland in yards
		int feetperyds = 3;	// number of feet in a yard
		int feetToMoon;	// number of feet to the moon
		int feetpersec = 2;	// feet travelled per second
		int secToWonderland; // number of seconds it took to get to Wonderland
		// Compute values:
		feetToMoon = ydsToWonderland * feetperyds;
		secToWonderland = feetToMoon / feetpersec;

		System.out.println("Distance to Wonderland in yards: " + ydsToWonderland);
		System.out.println("The number of feet per yard: " +feetperyds);
		System.out.println("The number of feet to Wonderland is " + feetToMoon);
		System.out.println("Number of seconds it takes to get to Wonderland travelling at 2 feet per second is " + secToWonderland);
	}
}
