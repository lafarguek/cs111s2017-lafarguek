//********************************************
// Kadeem LaFargue
// CMPSC 111
// 19 January 2017
// Lab 1
//
// Listing 1.1 from Lewis & Loftus, slightly modified.
// Demonstrates the basic structure of a Java application.
//********************************************

import java.util.Date;

public class Lab1Part2
{
	public static void main(String[] args)
	{
		System.out.println("Kadeem LaFargue" + new Date());
		System.out.println("Lab 1 Part 2");

		//-------------------------------
		//Print words of wisdom
		//-------------------------------

		System.out.println("I'm not telling you it's going to be easy, but I'm telling you it'll be worth it");
		System.out.println("Float like a butterfly sting like a bee");
	}
}

