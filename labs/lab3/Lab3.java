//*******************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kadeem LaFargue
//CMPSC 111 Spring 2017
//Lab #3
//Date: 02 02 2017
//
//Purpose: To calculate the tip and bill for the user.
//*******************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class Lab3
{
	//-------------------------
	//main method: Calculate tip and bill.
	//-------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Kadeem LaFargue\nLab 3\n" + new Date() + "\n");

		// Variable dictionary:
		double bill, tip, totalbill, percent;
		String name;
		Scanner scan = new Scanner (System.in);
		System.out.println ("What's your name?");
		name = scan.nextLine();

		System.out.println ("How much was your bill " + name + "?" );
		bill = scan.nextDouble();
		
		System.out.println ("What percentage would you like to tip " + name + "?");
		percent = scan.nextDouble();

		tip = (percent/100)*bill;

		totalbill = bill + tip;

		System.out.println (name + " your bill before tip is: $" + bill);
		System.out.println ("You're tipping: $" + tip);
		System.out.println ("Your total bill is: $" + totalbill);
	}
}
