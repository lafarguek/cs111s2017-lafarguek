//=================================================
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kadeem LaFargue
//CMPSC 111 Spring 2017
//Lab #4
//Date: 02 15 2017
//Purpose: 
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)
  {
	final int WIDTH = 600;
	final int HEIGHT = 400;

	//fill entire background cyan
	page.setColor(Color.cyan);
	page.fillRect(0,0,WIDTH, HEIGHT);

	//change color for the snow
        page.setColor(Color.white);

        //draw snow on the ground
        page.fillRect(0,HEIGHT - 80,WIDTH,HEIGHT); // Graphics Object #1: Rectangle

	//draw sun in corner
	page.setColor(Color.yellow);
	page.fillOval(0,0,50,50);	//Graphics Object #2: Oval

	//draw rays from sun
	page.drawLine(70,30,100,50); // Graphics Object #3: Line
        page.drawLine(30,70,50,100);
        page.drawLine(50,50,100,100);
        page.drawLine(70,10,150,15);
        page.drawLine(10,65,6,130);

	//House
	page.setColor(new Color(139,69,19));
	page.fillRect(200,200,200,140);

	//Roof
	page.setColor(Color.black);
	page.fillArc(200,170,200,60,0,180);	//Graphics Object #4: Arc

	//Door
	page.setColor(Color.green);
	page.fillRect(280,275,40,65);

	//Doorknob
	page.setColor(Color.magenta);
	page.fillOval(310,307,5,5);

	//Window
	page.setColor(Color.lightGray);
	page.fillRect(220,220,40,40);
	page.fillRect(340,220,40,40);
  }
}
