//*******************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kadeem LaFargue & Dylan Bish
//CMPSC 111 Spring 2017
//Practical 6
//Date: 03 31 2017
//
//Purpose: Generates a random number and has the user input numbers to guess it. The program will provide hints with each guess saying whether the user was too high or low.
//*******************************

import java.util.Scanner;
import java.util.Random;
import java.util.Date;
public class GuessMyNumber
{
	public static void main (String[] args)
	{
		System.out.println("Kadeem LaFargue\nPractical 6\n" + new Date() + "\n");
		Random rand = new Random();
		int value = rand.nextInt(100)+1;
		int input;
		Scanner scan = new Scanner(System.in);
		System.out.println("Guess the randomly generated number between 1 and 100");
		input = scan.nextInt();
		while (input != value)
		{
			if (input == value) {
				System.out.println("YOU GUESSED IT!");
			}
			else{
				System.out.println("You guessed the wrong number try again.");
			}
			if (input > value) {
				System.out.println("You guessed too high.");
			}
		        else{
				System.out.println("You guessed too low.");
			}
			input = scan.nextInt();
		}
	}
}

