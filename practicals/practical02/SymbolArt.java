//********************************
// Kadeem LaFargue
// Practical 2, January 27 2017
//
// Symbol Art
//********************************
import java.util.Date;
public class SymbolArt
{
	public static void main(String[] args)
	{
		System.out.println("Kadeem LaFargue, CMPSC 111\n" + new Date() + "\n");
		System.out.println("________      ________");
		System.out.println("|      |      |      |");
		System.out.println("|      |______|      |");
		System.out.println("|      ||    ||      |");
		System.out.println("|      ||    ||      |   ________");
		System.out.println("|      ||    ||      |   |      |");
		System.out.println("|      ||    ||      |   |      |");
		System.out.println("          New York City         ");
	}
}
