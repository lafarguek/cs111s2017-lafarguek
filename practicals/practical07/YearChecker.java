//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

import java.util.Scanner;

public class YearChecker
{
    //Create instance variables
    int year, userInput;

    //Create a constructor
    public YearChecker(int y)
    {
        year = userInput;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear()
    {
        if (year % 4 == 0)
	{
		System.out.println("It is a Leap Year.");
	}
	else if (year % 400 == 0)
	{
		System.out.println("It is a Leap Year.");
	}
	else {
		System.out.println("It is not a Leap Year.");
	}
    }

    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear()
    {
        if (year % 17 == 0)
	{
		System.out.println("This is an emergence year for Brood II.");
	}
	else {
		System.out.println("This is not an emergence year for Brood II."); }
    }

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear()
    {
        if (year % 11 == 0)
	{
		System.out.println("This year is a peak sunspot year.");
	}
	else {
		System.out.println("This year is not a peak sunspot year.");
	}
    }
}
