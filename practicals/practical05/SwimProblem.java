//*******************************
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
//Kadeem LaFargue
//CMPSC 111 Spring 2017
//Practical 5
//Date: 02 24 2017
//
//Purpose: Accepts inputs and puts them into a story and solves the problem.
//*******************************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class SwimProblem
{
	//-------------------------
	//main method: program execution begins here
	//-------------------------
	public static void main(String[] args)
	{
		// Variable dictionary:
		double First25, Second25, FinT;
		String name;
		Scanner scan = new Scanner (System.in);
		System.out.println("Please enter your name");
		name = scan.nextLine();

		System.out.println("Enter your first and second 25 splits " + name + ".");
		First25 = scan.nextDouble();
		Second25 = scan.nextDouble();

		FinT = First25 + Second25;

		System.out.println("Your 25 splits in your 50 were " + First25 + " for your first 25 and " + Second25 + " for your second 25. Your final 50 time was " + FinT + ".");
	}
}
