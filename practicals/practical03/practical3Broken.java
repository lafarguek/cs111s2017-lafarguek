
//************
//This is the Honor Code Pledge Stating the Following work is mine unless otherwise cited
//Michael MacDougall 1271409
//CS111S2017
//Practical 2
//Date: 27 January 2017 
//
//Purpose Printing something interesting
//**************

import java.util.Date; //Will import a current date based off the system date.
public class practical3Broken // Fixed the public class.

{
	//main method: execution begins here
	//
	public static void main(String[] args) //Took away the semicolon.
	{
		//Label Output with name and date for every program.
		System.out.println("Michael MacDougall \nPractical 2\n" + new Date() + "\n");


		System.out.println("++==========//=====\\----    * * * * * * * *    //--\\");
		System.out.println("      ++==(  -0-  )       			|. |");
		System.out.println("++==========\\=====//----   * * * * * * * *     \\__//");
		System.out.print(" \"To Boldly Go Where No Man Has Gone Before\" "); //I typed this solely to demonstrate I understand how the \n special characters work in Java.  //Fixed the picture by adding double back slashs and adding a quotation mark to the quote to fix it/

		// Variable Directory Goes here.
	}
}


// I changed the public class name. Program won't run unless it matches, and that's a common problem if people don't understand that fact, also I put a semi colon after the the main method line, which doesn't work. It will compile but I changed a println statement to a print statement, that makes the final product look wrong. I messed around and broke some of the \ statements in the lines of code to make the picture not work properly
//
